import React, { Component } from 'react';
import './App.css';
import ValidationComponent from './components/Validation';
import Char from './components/Char';

class App extends Component {
  state = {
    text: ''
  }

  textHandler = (e) => {
    this.setState({
      text: e.target.value
    })
  }

  removeHandler = (index) => {
    const textArr = [...this.state.text]
    textArr.splice(index, 1)
    this.setState({
      text: textArr.join('')
    })
  }

  render() {
    const characters = [...this.state.text].map((item, index) => {
      return (<Char key={index} character={item} click={() => this.removeHandler(index)}/>)
    })

    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <input
          type="text"
          placeholder="Type something"
          onChange={this.textHandler}
          value={this.state.text}
        />
        <p>{this.state.text.length}</p>
        <ValidationComponent textLength={this.state.text.length} />
        {characters}
      </div>
    );
  }
}

export default App;
